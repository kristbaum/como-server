# coding: utf-8

from __future__ import absolute_import

from datetime import date, datetime  # noqa: F401
from typing import List, Dict  # noqa: F401

import util
from models.base_model_ import Model
from models.language import Language  # noqa: F401,E501


class DataUser(Model):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    def __init__(self, version: int=None, client: str=None, lemma_lang: Language=None, sense_lang: Language=None):  # noqa: E501
        """DataUser - a model defined in Swagger

        :param version: The version of this DataUser.  # noqa: E501
        :type version: int
        :param client: The client of this DataUser.  # noqa: E501
        :type client: str
        :param lemma_lang: The lemma_lang of this DataUser.  # noqa: E501
        :type lemma_lang: Language
        :param sense_lang: The sense_lang of this DataUser.  # noqa: E501
        :type sense_lang: Language
        """
        self.swagger_types = {
            'version': int,
            'client': str,
            'lemma_lang': Language,
            'sense_lang': Language
        }

        self.attribute_map = {
            'version': 'version',
            'client': 'client',
            'lemma_lang': 'lemma_lang',
            'sense_lang': 'sense_lang'
        }
        self._version = version
        self._client = client
        self._lemma_lang = lemma_lang
        self._sense_lang = sense_lang

    @classmethod
    def from_dict(cls, dikt) -> 'DataUser':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The Data_user of this DataUser.  # noqa: E501
        :rtype: DataUser
        """
        return util.deserialize_model(dikt, cls)

    @property
    def version(self) -> int:
        """Gets the version of this DataUser.


        :return: The version of this DataUser.
        :rtype: int
        """
        return self._version

    @version.setter
    def version(self, version: int):
        """Sets the version of this DataUser.


        :param version: The version of this DataUser.
        :type version: int
        """
        if version is None:
            raise ValueError("Invalid value for `version`, must not be `None`")  # noqa: E501

        self._version = version

    @property
    def client(self) -> str:
        """Gets the client of this DataUser.


        :return: The client of this DataUser.
        :rtype: str
        """
        return self._client

    @client.setter
    def client(self, client: str):
        """Sets the client of this DataUser.


        :param client: The client of this DataUser.
        :type client: str
        """
        if client is None:
            raise ValueError("Invalid value for `client`, must not be `None`")  # noqa: E501

        self._client = client

    @property
    def lemma_lang(self) -> Language:
        """Gets the lemma_lang of this DataUser.


        :return: The lemma_lang of this DataUser.
        :rtype: Language
        """
        return self._lemma_lang

    @lemma_lang.setter
    def lemma_lang(self, lemma_lang: Language):
        """Sets the lemma_lang of this DataUser.


        :param lemma_lang: The lemma_lang of this DataUser.
        :type lemma_lang: Language
        """
        if lemma_lang is None:
            raise ValueError("Invalid value for `lemma_lang`, must not be `None`")  # noqa: E501

        self._lemma_lang = lemma_lang

    @property
    def sense_lang(self) -> Language:
        """Gets the sense_lang of this DataUser.


        :return: The sense_lang of this DataUser.
        :rtype: Language
        """
        return self._sense_lang

    @sense_lang.setter
    def sense_lang(self, sense_lang: Language):
        """Sets the sense_lang of this DataUser.


        :param sense_lang: The sense_lang of this DataUser.
        :type sense_lang: Language
        """
        if sense_lang is None:
            raise ValueError("Invalid value for `sense_lang`, must not be `None`")  # noqa: E501

        self._sense_lang = sense_lang
