# coding: utf-8

from __future__ import absolute_import

from datetime import date, datetime  # noqa: F401
from typing import List, Dict  # noqa: F401

import util
from models.base_model_ import Model
from models.server_data_games import ServerDataGames  # noqa: F401,E501
from models.server_data_sense_creations import ServerDataSenseCreations  # noqa: F401,E501
from models.server_data_user import ServerDataUser  # noqa: F401,E501


class ServerData(Model):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    def __init__(self, games: List[ServerDataGames]=None, sense_creations: List[ServerDataSenseCreations]=None, user: ServerDataUser=None):  # noqa: E501
        """ServerData - a model defined in Swagger

        :param games: The games of this ServerData.  # noqa: E501
        :type games: List[ServerDataGames]
        :param sense_creations: The sense_creations of this ServerData.  # noqa: E501
        :type sense_creations: List[ServerDataSenseCreations]
        :param user: The user of this ServerData.  # noqa: E501
        :type user: ServerDataUser
        """
        self.swagger_types = {
            'games': List[ServerDataGames],
            'sense_creations': List[ServerDataSenseCreations],
            'user': ServerDataUser
        }

        self.attribute_map = {
            'games': 'games',
            'sense_creations': 'sense_creations',
            'user': 'user'
        }
        self._games = games
        self._sense_creations = sense_creations
        self._user = user

    @classmethod
    def from_dict(cls, dikt) -> 'ServerData':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The ServerData of this ServerData.  # noqa: E501
        :rtype: ServerData
        """
        return util.deserialize_model(dikt, cls)

    @property
    def games(self) -> List[ServerDataGames]:
        """Gets the games of this ServerData.


        :return: The games of this ServerData.
        :rtype: List[ServerDataGames]
        """
        return self._games

    @games.setter
    def games(self, games: List[ServerDataGames]):
        """Sets the games of this ServerData.


        :param games: The games of this ServerData.
        :type games: List[ServerDataGames]
        """

        self._games = games

    @property
    def sense_creations(self) -> List[ServerDataSenseCreations]:
        """Gets the sense_creations of this ServerData.


        :return: The sense_creations of this ServerData.
        :rtype: List[ServerDataSenseCreations]
        """
        return self._sense_creations

    @sense_creations.setter
    def sense_creations(self, sense_creations: List[ServerDataSenseCreations]):
        """Sets the sense_creations of this ServerData.


        :param sense_creations: The sense_creations of this ServerData.
        :type sense_creations: List[ServerDataSenseCreations]
        """

        self._sense_creations = sense_creations

    @property
    def user(self) -> ServerDataUser:
        """Gets the user of this ServerData.


        :return: The user of this ServerData.
        :rtype: ServerDataUser
        """
        return self._user

    @user.setter
    def user(self, user: ServerDataUser):
        """Sets the user of this ServerData.


        :param user: The user of this ServerData.
        :type user: ServerDataUser
        """

        self._user = user
