FROM python:3

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/

RUN pip3 install --no-cache-dir -r requirements.txt

COPY . /usr/src/app

EXPOSE 5000

ENTRYPOINT ["uwsgi"]
#CMD ["--help"]
CMD ["--http", ":5000", "--file", "como_server/app.py", "--master", "-p", "2", "-t", "2", "--enable-threads"]
# python3-dev