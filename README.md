# como-server

Server for the Como-App (Beta)

##Installation on Toolforge

Clone the repo to the toolforge account:

`git clone https://gitlab.com/kristbaum/como-server.git`

Create the necessary folders:

`mkdir -p $HOME/www/python/src` 

Symlink to the default entry point

`ln -s $HOME/como-server/como_server/app.py $HOME/www/python/src/app.py`

Start the webservice with:

`webservice --backend=kubernetes python3.7 start`

Create a Python venv with these [instructions](https://wikitech.wikimedia.org/wiki/Help:Toolforge/Web/Python#Creating_a_virtual_environment).

Create config.py with these parameters:
* database = ""
* user = ""
* host = ""
* port = ""
* password = ""

Database:

CREATE DATABASE "TOOLFORGE-USERNAME"__como_p
USE "TOOLFORGE-USERNAME"__como_p

Create a bashscript update.sh with:


```#!/bin/bash
cd www/python/src
git pull
cd ..
cd ..
cd ..
webservice restart
sleep 2
tail -n 70 uwsgi.log 
```


Create local DB for testing:
docker run -p 127.0.0.1:3306:3306  --name some-mariadb -e MARIADB_ROOT_PASSWORD=SUPERSECREPASS -d mariadb:latest