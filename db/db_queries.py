import logging
import pymysql


class DBQueries:
    def __init__(self, db):
        self.cur = db.conn
        #self.conn = db.conn
        self.db = db

    def delete_table(self):
        self.cur.execute("DROP TABLE games;")

    def insert_game(self, game_list):
        self.cur.execute(self.db.games.insert(), game_list)

    def insert_sense_creation(self, sense_list):
        self.cur.execute(self.db.sense_creations.insert(), sense_list)

    def commit(self):
        self.conn.commit()

    def rollback(self):
        self.conn.rollback()

    def does_user_exist(self, user_id):
        try:
            self.cur.execute("SELECT uuid FROM users WHERE uuid = %s", (user_id,))
            results = self.cur.fetchall()
            if len(results) > 0:
                return 1
            else:
                return 0
        except pymysql.Error as e:
            self.conn.rollback()
            logging.error(e)
            logging.error("DB Error")
            logging.error("Try reconnection to DB")
            self.db.connect_to_db()
            try:
                logging.info("Retry user check")
                self.cur.execute("SELECT uuid FROM users WHERE uuid = %s", (user_id,))
                results = self.cur.fetchall()
                if len(results) > 0:
                    return 1
                else:
                    return 0
            except pymysql.Error as e:
                logging.error(e)
                logging.error("Seems to be a grave error with the DB connection")
                return 2

    def insert_user(self, uuid, lemma_lang, sense_lang):
        self.cur.execute(self.db.users.insert(), [
            {'uuid': uuid},
            {'user_lemma_lang': lemma_lang},
            {'user_sense_lang': sense_lang}])
        logging.info("Create new user: " + uuid)
        self.commit()

    def read_new_games(self, uuid, lemma_lang, sense_lang, amount):
        try:
            #self.db.games.select()
            self.cur.execute("SELECT games.id, games.l, games.category, games.lemma, games.lemma_lang, "
                             "games.sense, games.sense_lang "
                             "FROM games "
                             "WHERE games.lemma_lang = %s "
                             "AND games.sense_lang = %s "
                             "AND NOT games.id IN "
                             "(SELECT games.id "
                             "FROM games_to_users INNER JOIN games "
                             "ON games.id = games_to_users.gameid "
                             "WHERE games_to_users.userid = %s OR games.created_by = %s) "
                             "ORDER BY priority DESC, RANDOM() LIMIT %s;", (lemma_lang, sense_lang, uuid, uuid, amount))
            results = self.cur.fetchall()
        except pymysql.ProgrammingError:
            logging.error("Probably no more results available")
            results = []
        for result in results:
            self.cur.execute("INSERT INTO games_to_users (userid, gameid) "
                             "VALUES (%s, %s);", (uuid, result[0]))
        self.commit()
        return results

    def read_new_sensecreations(self, uuid, lemma_lang, amount):
        try:
            self.cur.execute("SELECT sense_creation_id, l, category, lemma, lemma_lang "
                             "FROM sense_creations "
                             "WHERE lemma_lang = %s "
                             "AND NOT sense_creation_id IN "
                             "(SELECT sense_creations.sense_creation_id "
                             "FROM sense_creations_to_users INNER JOIN sense_creations "
                             "ON sense_creations.sense_creation_id = sense_creations_to_users.sense_creation_id "
                             "WHERE sense_creations_to_users.userid = %s) "
                             "ORDER BY RANDOM() LIMIT %s;", (lemma_lang, uuid, amount))
            results = self.cur.fetchall()
        except pymysql.ProgrammingError:
            logging.error("Probably no more results available")
            results = []
        for result in results:
            self.cur.execute("INSERT INTO sense_creations_to_users (userid, sense_creation_id) "
                             "VALUES (%s, %s);", (uuid, result[0]))
        self.commit()
        return results

    def insert_performance_data(self, uuid, game_id, input_lang, inputs, skipped, solved, tries):
        update_error = False
        try:
            self.cur.execute("UPDATE games_to_users "
                             "SET skipped = %s, solved = %s, tries = %s, received_at = NOW() "
                             "WHERE gameid = %s AND userid = %s;",
                             (skipped, solved, tries, game_id, uuid))

            # Remove duplicate entries
            inputs = list(set(inputs))

            for p_input in inputs:
                self.cur.execute(self.db.inputs.insert(), [
                    {'input': p_input},
                    {'lang': input_lang},
                    {'userid': uuid},
                    {'gameid': game_id}])
        except pymysql.Error as e:
            logging.error("Database missed didn't track initial sending of games (Probably deleted database)")
            update_error = True
            logging.error(e)
            self.rollback()

        # Award points
        if solved and not update_error:
            self.cur.execute("SELECT users.uuid, users.points "
                             "FROM users "
                             "WHERE users.uuid = "
                             "(SELECT created_by FROM games WHERE games.id = %s LIMIT 1);", (game_id,))
            result = self.cur.fetchone()

            if result is not None:
                self.cur.execute("UPDATE users SET points = %s WHERE users.uuid = %s;",
                                 (result[1] + 1, result[0]))

    def insert_sense_creation_inputs(self, uuid, sense_creation_id, skipped, sense_lang, sense_input):
        # Todo: check for "bad" words
        update_error = False
        try:
            self.cur.execute("UPDATE sense_creations_to_users "
                             "SET player_input = %s, skipped = %s, sense_lang = %s, received_at = NOW()"
                             "WHERE sense_creation_id = %s AND userid = %s;",
                             (sense_input, skipped, sense_lang, sense_creation_id, uuid))
        except pymysql.Error as e:
            logging.error("Update to sense_creations failed")
            update_error = True
            logging.error(e)
            self.rollback()

        if not skipped and not update_error:
            self.create_new_game(uuid, sense_creation_id, sense_lang, sense_input)

    def create_new_game(self, uuid, sense_creation_id, sense_lang, sense_input):
        self.cur.execute("SELECT l, category, lemma, lemma_lang "
                         "FROM sense_creations WHERE sense_creation_id = %s", (sense_creation_id,))
        result = self.cur.fetchone()

        l_number = result[0]
        category = result[1]
        lemma = result[2]
        lemma_lang = result[3]

        logging.info("Create new game from player_input: " + sense_input)
        self.cur.execute(self.db.games.insert(), [
            {'l': l_number},
            {'category': category},
            {'lemma': lemma},
            {'lemma_lang': lemma_lang},
            {'sense': sense_input},
            {'sense_lang': sense_lang},
            {'sense_id': sense_creation_id},
            {'created_by': uuid},
            {'priority': 50}])

    def insert_user_data(self, uuid, version, client, user_lemma_lang, user_sense_lang):
        try:
            self.cur.execute("UPDATE users "
                             "SET version = %s, client = %s, user_lemma_lang = %s, user_sense_lang = %s "
                             "WHERE users.uuid = %s;",
                             (version, client, user_lemma_lang, user_sense_lang, uuid))
        except pymysql.Error:
            logging.error("Userdata couldn't be updated")

    def read_userdata(self, user_id):
        self.cur.execute("SELECT points FROM users WHERE users.uuid = %s", (user_id,))
        result = self.cur.fetchone()
        obj = {
            "points": result[0]
        }
        return obj
