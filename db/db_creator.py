import logging
import time

import pymysql
import config
import sqlalchemy
from sqlalchemy import MetaData, Table, Column, Integer, TIMESTAMP, String, Boolean, ForeignKey
from sqlalchemy.ext.declarative import declarative_base


class DB:
    def __init__(self):
        self.users = None
        self.sense_creations = None
        self.sense_creations_to_users = None
        self.games = None
        self.games_to_users = None
        self.inputs = None

        self.engine = sqlalchemy.create_engine("mysql+pymysql://%s:%s@%s/%s" %
                                               (config.user,
                                                config.password,
                                                config.host,
                                                config.database),
                                               echo=True)
        self.conn = self.engine.connect()
        self.create_alchemy_table()
        Base = declarative_base()
        users = Users(Base)

    def connect_to_db(self):
        try:
            self.conn = pymysql.connect(database=config.database,
                                        user=config.user,
                                        host=config.host,
                                        port=config.port,
                                        password=config.password)

            # Open a cursor to perform database operations
            self.cur = self.conn.cursor()
        except pymysql.Error as e:
            logging.error("DB conn failed, retry in 60 sec")
            time.sleep(60)
            self.connect_to_db()

    def close_connection(self):
        self.cur.close()
        self.conn.close()

    def create_alchemy_table(self):
        meta = MetaData()
        self.users = Table(
            'users', meta,
            Column('uuid', String(length=36), primary_key=True),
            Column('client', String(length=255)),
            Column('user_lemma_lang', String(length=2)),
            Column('user_sense_lang', String(length=2)),
            Column('version', Integer),
            Column('points', Integer, default=0),
            Column('created_at', TIMESTAMP, nullable=False, server_default=sqlalchemy.func.now())
        )
        self.sense_creations = Table(
            'sense_creations', meta,
            Column('sense_creation_id', Integer, autoincrement=True, nullable=False, primary_key=True),
            Column('l', String(length=255), nullable=False),
            Column('category', String(length=255), nullable=False),
            Column('lemma', String(length=510), nullable=False),
            Column('lemma_lang', String(length=2), nullable=False),
            Column('created_at', TIMESTAMP, nullable=False, server_default=sqlalchemy.func.now())
        )
        self.sense_creations_to_users = Table(
            'sense_creations_to_users', meta,
            Column('created_at', TIMESTAMP, nullable=False, server_default=sqlalchemy.func.now()),
            Column('received_at', TIMESTAMP, nullable=False, default=None),
            Column('userid', String(length=36), ForeignKey('users.uuid'), primary_key=True),
            Column('sense_creation_id', Integer, ForeignKey('sense_creations.sense_creation_id'), primary_key=True),
            Column('player_input', String(length=5000)),
            Column('skipped', Boolean),
            Column('sense_lang', String(length=2))
        )
        self.games = Table(
            'games', meta,
            Column('id', Integer, autoincrement=True, nullable=False, primary_key=True),
            Column('l', String(length=36), nullable=False, default=None),
            Column('category', String(length=5000), nullable=False),
            Column('lemma', String(length=5000), nullable=False),
            Column('lemma_lang', String(length=2), nullable=False),
            Column('sense', String(length=5000), nullable=False),
            Column('sense_lang', String(length=2), nullable=False),
            Column('sense_id', Integer, ForeignKey('sense_creations.sense_creation_id')),
            Column('created_by', String(length=36), ForeignKey('users.uuid')),
            Column('priority', Integer, default=10),
            Column('created_at', TIMESTAMP, nullable=False, server_default=sqlalchemy.func.now())
        )
        self.games_to_users = Table(
            'games_to_users', meta,
            Column('skipped', Boolean),
            Column('solved', Boolean),
            Column('tries', Integer),
            Column('sent_at', TIMESTAMP, nullable=False, server_default=sqlalchemy.func.now()),
            Column('received_at', TIMESTAMP, nullable=True),
            Column('userid', String(length=36), ForeignKey('users.uuid'), primary_key=True),
            Column('gameid', Integer, ForeignKey('games.id'), primary_key=True)
        )
        self.inputs = Table(
            'inputs', meta,
            Column('id', Integer, autoincrement=True, primary_key=True),
            Column('input', String(5000)),
            Column('lang', String(2)),
            Column('userid', String(36), ForeignKey('users.uuid')),
            Column('gameid', Integer, ForeignKey('games.id'))
        )
        meta.create_all(bind=self.engine)

    # https://www.postgresql.org/docs/9.5/datatype-json.html#JSON-TYPE-MAPPING-TABLE
    def create_tables(self):
        self.cur.execute("CREATE TABLE IF NOT EXISTS users ("
                         "uuid VARCHAR(36) PRIMARY KEY, "
                         "client TEXT, "
                         "user_lemma_lang TEXT, "
                         "user_sense_lang TEXT, "
                         "version INTEGER, "
                         "points INTEGER DEFAULT 0,"
                         "created_at DATETIME DEFAULT CURRENT_TIMESTAMP);")
        self.cur.execute("CREATE TABLE IF NOT EXISTS sense_creations ("
                         "sense_creation_id SERIAL PRIMARY KEY, "
                         "l TEXT NOT NULL, "
                         "category TEXT NOT NULL, "
                         "lemma TEXT NOT NULL, "
                         "lemma_lang TEXT NOT NULL, "
                         "created_at DATETIME DEFAULT CURRENT_TIMESTAMP);")
        self.cur.execute("CREATE TABLE IF NOT EXISTS sense_creations_to_users ("
                         "created_at DATETIME DEFAULT CURRENT_TIMESTAMP, "
                         "received_at DATETIME DEFAULT NULL, "
                         "userid VARCHAR(36) REFERENCES users(uuid), "
                         "sense_creation_id BIGINT UNSIGNED REFERENCES sense_creations(sense_creation_id), "
                         "player_input TEXT, "
                         "skipped BOOLEAN, "
                         "sense_lang TEXT, "
                         "PRIMARY KEY (userid, sense_creation_id));")
        self.cur.execute("CREATE TABLE IF NOT EXISTS games ("
                         "id SERIAL PRIMARY KEY, "
                         "l TEXT NOT NULL, "
                         "category TEXT NOT NULL, "
                         "lemma TEXT NOT NULL, "
                         "lemma_lang TEXT NOT NULL, "
                         "sense TEXT NOT NULL, "
                         "sense_lang TEXT NOT NULL, "
                         "sense_id BIGINT UNSIGNED REFERENCES sense_creations(sense_creation_id), "
                         "created_by VARCHAR(36) REFERENCES users(uuid), "
                         "priority INTEGER DEFAULT 10, "
                         "created_at DATETIME DEFAULT CURRENT_TIMESTAMP);")
        self.cur.execute("CREATE TABLE IF NOT EXISTS games_to_users ("
                         "skipped BOOLEAN, "
                         "solved BOOLEAN, "
                         "tries INTEGER, "
                         "sent_at DATETIME DEFAULT CURRENT_TIMESTAMP, "
                         "received_at DATETIME DEFAULT NULL, "
                         "userid VARCHAR(36) REFERENCES users(uuid), "
                         "gameid BIGINT UNSIGNED REFERENCES games(id), "
                         "PRIMARY KEY (userid, gameid));")
        self.cur.execute("CREATE TABLE IF NOT EXISTS inputs ("
                         "id SERIAL PRIMARY KEY, "
                         "input TEXT, "
                         "lang TEXT, "
                         "userid VARCHAR(36) REFERENCES users(uuid), "
                         "gameid BIGINT UNSIGNED REFERENCES games(id));")
        self.conn.commit()


class Users(Base):
    __tablename__ = "users"

    uuid = Column('uuid', String(36), primary_key=True)
    client = Column('client', String(255))
    user_lemma_lang = Column('user_lemma_lang', String(2))
    user_sense_lang = Column('user_sense_lang', String(2))
    version = Column('version', Integer)
    points = Column('points', Integer, default=0)
    created_at = Column('created_at', TIMESTAMP, nullable=False, server_default=sqlalchemy.func.now())
