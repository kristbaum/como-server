import logging
import time

import pymysql
import sqlalchemy


class Worker:
    def __init__(self, db):
        self.db = db
        while True:
            self.work()
            time.sleep(580)

    def work(self):
        try:
            games = self.db.games
            s = self.db.games.select(games.c.lemma)
            result = self.db.conn.execute(s).fetchall()
            sqlalchemy.select([games])
            print("Results: ")
            print(result)
            #self.db.conn.execute("SELECT * FROM games")
            #results = self.db.conn.fetchall()
        except pymysql.Error as e:
            logging.error("DB Error")
            logging.error("Try reconnection to DB")
            self.db.connect_to_db()






