import logging
import random
from .wdquery import WDQuery


class GameCreator:
    def __init__(self, db):
        self.db = db
        self.languages = ["en", "de"]
        # Activate only on empty table
        # self.query_existing_senses()
        # self.query_nonexisting_senses()

    def generate_query_existing_senses(self, lang):
        return """SELECT ?l ?lemma ?sense ?cat WHERE {
                         ?l a ontolex:LexicalEntry ;
                         dct:language ?language ;
                         wikibase:lemma ?lemma ;
                         wikibase:lexicalCategory ?cat ;
                         ontolex:sense/skos:definition ?sense. 
                         ?language wdt:P218 """ + "'{0}'".format(lang) + ". }"

    def query_existing_senses(self):
        logging.info("Create new games")
        for language in self.languages:
            query = self.generate_query_existing_senses(language)
            results = self.query(query)
            clean_results = results["results"]["bindings"]
            random.shuffle(clean_results)
            game_list = []
            for result in clean_results:
                sense_lang = result["sense"]["xml:lang"]
                lemma_lang = result["lemma"]["xml:lang"]
                if (sense_lang == "de" or sense_lang == "en") and (lemma_lang == "de" or lemma_lang == "en"):
                    # strip urls from http://www.wikidata.org/entity/
                    l_number = result["l"]["value"].rsplit('/', 1)[-1]
                    category = result["cat"]["value"].rsplit('/', 1)[-1]
                    lemma = result["lemma"]["value"]

                    sense = result["sense"]["value"]
                    game_obj = {'l': l_number,
                                'category': category,
                                'lemma': lemma,
                                'lemma_lang': lemma_lang,
                                'sense': sense,
                                'sense_lang': sense_lang}
                    game_list.append(game_obj)
            self.db.insert_game(game_list)

    def generate_query_nonexisting_senses(self, lang):
        return """SELECT ?l ?lemma ?cat WHERE {
                             ?l a ontolex:LexicalEntry ; 
                             dct:language ?language ;
                             wikibase:lexicalCategory ?cat ;
                             wikibase:lemma ?lemma .
                             ?language wdt:P218 """ + "'{0}'".format(lang) + """
                             FILTER NOT EXISTS {?l ontolex:sense ?sense }
                         }"""

    def query_nonexisting_senses(self):
        logging.info("Create new sense_creations")
        for language in self.languages:
            query = self.generate_query_nonexisting_senses(language)

            results = self.query(query)
            clean_results = results["results"]["bindings"]
            random.shuffle(clean_results)

            sense_list = []
            for result in clean_results:
                lemma_lang = result["lemma"]["xml:lang"]
                if lemma_lang == "de" or lemma_lang == "en":
                    # strip urls from http://www.wikidata.org/entity/
                    l_number = result["l"]["value"].rsplit('/', 1)[-1]
                    category = result["cat"]["value"].rsplit('/', 1)[-1]
                    lemma = result["lemma"]["value"].rsplit('/', 1)[-1]
                    lemma_lang = result["lemma"]["xml:lang"]
                    sense_obj = {'l': l_number,
                                 'category': category,
                                 'lemma': lemma,
                                 'lemma_lang': lemma_lang}
                    sense_list.append(sense_obj)
            self.db.insert_sense_creation(sense_list)

    def query(self, query):
        endpoint_url = "https://query.wikidata.org/sparql"
        wdquery = WDQuery()
        try:
            return wdquery.get_results(endpoint_url, query)
        except BaseException as e:
            logging.warning(e)
