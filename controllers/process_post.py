import logging


class ProcessPost:
    db_queries = None

    def __init__(self, db_queries):
        ProcessPost.db_queries = db_queries


def process(data):
    logging.info("Received POST data: " + str(data))

    db = ProcessPost.db_queries
    uuid = data.id
    version = data.user.version
    client = data.user.client
    user_lemma_lang = data.user.lemma_lang
    user_sense_lang = data.user.sense_lang
    if db.does_user_exist(uuid) == 0:
        db.insert_user(uuid, user_lemma_lang, user_sense_lang)
    elif db.does_user_exist(uuid) == 2:
        logging.error("Terminating processing of POST data")
        return

    db.insert_user_data(uuid, version, client, user_lemma_lang, user_sense_lang)
    for game in data.game_data:
        game_id = game.id
        input_lang = game.input_lang
        skipped = game.skipped
        solved = game.solved
        tries = game.tries
        inputs = game.player_inputs
        db.insert_performance_data(uuid, game_id, input_lang, inputs, skipped, solved, tries)

    for sense_creation in data.sense_creation_data:
        sense_creation_id = sense_creation.id
        sense_input = sense_creation.sense_input
        sense_lang = sense_creation.sense_lang
        skipped = sense_creation.skipped
        db.insert_sense_creation_inputs(uuid, sense_creation_id, skipped, sense_lang, sense_input)
    db.commit()


