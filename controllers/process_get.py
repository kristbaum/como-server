class ProcessGet:
    db_queries = None

    def __init__(self, db_queries):
        ProcessGet.db_queries = db_queries


def process(uuid, lemma, sense):
    db = ProcessGet.db_queries

    if db.does_user_exist(uuid) == 0:
        db.insert_user(uuid, lemma, sense)
    elif db.does_user_exist(uuid) == 2:
        obj = {
            "status": "db_error"
        }
        return obj

    games = db.read_new_games(uuid, lemma, sense, 15)
    formatted_games = objectify(games, True)
    sense_creations = db.read_new_sensecreations(uuid, lemma, 10)
    formatted_sense_creations = objectify(sense_creations, False)
    userdata = db.read_userdata(uuid)
    obj = {
        "games": formatted_games,
        "sense_creations": formatted_sense_creations,
        "user": userdata
    }
    return obj


def objectify(array, game):
    result = []
    for item in array:
        l_obj = {
            "type": "uri",
            "value": "http://www.wikidata.org/entity/" + item[1]
        }
        cat_obj = {
            "type": "uri",
            "value": "http://www.wikidata.org/entity/" + item[2]
        }
        lemma_obj = {
            "type": "literal",
            "value": item[3],
            "xml:lang": item[4]
        }
        if game:
            sense_obj = {
                "type": "literal",
                "value": item[5],
                "xml:lang": item[6]
            }
            sub_obj = {
                "l": l_obj,
                "sense": sense_obj,
                "cat": cat_obj,
                "lemma": lemma_obj
            }
        else:
            sub_obj = {
                "l": l_obj,
                "cat": cat_obj,
                "lemma": lemma_obj
            }
        obj = {
            "id": item[0],
            "data": sub_obj
        }
        result.append(obj)
    return result

