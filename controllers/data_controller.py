import logging
import connexion
from . import process_get, process_post
from models.data import Data
from models.server_data import ServerData


def download_data(uuid, lemma, sense):  # noqa: E501
    """Download game data

    Download new games and lemmas # noqa: E501

    :param uuid: UUID
    :type uuid: str
    :param lemma: Language of the requested lemmas
    :type lemma: str
    :param sense: Language of the requested senses
    :type sense: str

    :rtype: ServerData
    """
    logging.info("Received GET")
    if (uuid is not None) and (uuid != "null"):
        return process_get.process(uuid, lemma, sense)
    else:
        obj = {
            "status": "uuid=null"
        }
        return obj


def upload_data(body):  # noqa: E501
    """Upload player data

    Uploads inputs and performance data from players # noqa: E501

    :param body: Gamedata from players
    :type body: dict | bytes

    :rtype: None
    """
    logging.info("Received POST")

    if connexion.request.is_json:
        body = Data.from_dict(connexion.request.get_json())  # noqa: E501

        uuid = body.id
        if (uuid is not None) and (uuid != "null"):
            process_post.process(body)
            obj = {
                "status": "processed"
            }
        else:
            obj = {
                "status": "uuid_null"
            }
        return obj
    else:
        obj = {
            "status": "not_json"
        }
        return obj
