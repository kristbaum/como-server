#!/usr/bin/env python3
import app as application

if __name__ == '__main__':
    application = application.run()
