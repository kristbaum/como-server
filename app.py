import logging
import socket
import sys
import threading

import connexion

import controllers.process_get
import create.creategame
from controllers.process_post import ProcessPost
from create import worker_thread
from db import db_creator, db_queries
from encoder import JSONEncoder


def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        ip = s.getsockname()[0]
    except Exception:
        ip = '127.0.0.1'
    finally:
        s.close()
    return ip


logging.basicConfig(level=logging.INFO)  # filename='como-server.log'
logging.info("Start")


logging.info("Start db connector")
db_instance = db_creator.DB()
db_queries_instance = db_queries.DBQueries(db_instance)
controllers.process_get.ProcessGet(db_queries_instance)
ProcessPost(db_queries_instance)

logging.info("Start worker thread")
t1 = threading.Thread(target=worker_thread.Worker, args=[db_instance])
t1.start()

game_creator = create.creategame.GameCreator(db_queries_instance)

    # signal.signal(signal.SIGINT, exit)
    # atexit.register(custom_exit)
options = {"swagger_ui": True}
app = connexion.App(__name__, specification_dir='swagger/', options=options)
app.app.json_encoder = JSONEncoder
app.add_api('openapi.yaml', arguments={'title': 'Como'}, pythonic_params=True)


if get_ip() == '192.168.1.84':
    logging.info("Running on 84")
    app.run(host="192.168.1.84", port=5000)
else:
    logging.info("Running with uWSGI")
# app.run(host="192.168.1.180", port=5000)


def custom_exit():
    logging.warning("Exit gracefully")
    db_instance.close_connection()
    sys.exit(0)
